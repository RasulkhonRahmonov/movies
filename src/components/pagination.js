import React from 'react';

class Pagination extends React.Component {
    constructor(props) {
        super(props);

        this.prevPage = this.prevPage.bind(this);
        this.nextPage = this.nextPage.bind(this);
    }

    prevPage() {
        this.props.changePage(-1);
    }

    nextPage() {
        this.props.changePage(1);
    }

    render() {
        return (
            <nav aria-label="Page navigation example">
                <ul className="pagination justify-content-center mt-5">
                    <li className={`page-item ${this.props.page === 1 ? 'disabled' : ''}`}>
                        <button className="page-link" onClick={this.prevPage}>Previous</button>
                    </li>
                    <li className={`page-item ${this.props.page === this.props.total_pages ? 'disabled' : ''}`}>
                        <button className="page-link" onClick={this.nextPage}>Next</button>
                    </li>
                </ul>
            </nav>
        )
    }
}

export default Pagination;