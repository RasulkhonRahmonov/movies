import React from 'react'
import { Link } from 'react-router-dom';

export default class Movies extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        const movieList = this.props.movies.map(movie => (
            <div key={movie.id} className="col-md-3 mt-5">
                <h3 className='title'>{movie.title}</h3>
                <Link to={`/${movie.id}`}>
                    <img src={`https://image.tmdb.org/t/p/w500${movie.poster_path}`}
                         alt='photo'
                         width="300"
                         className='img'/>
                </Link>
            </div>
        ));

        if (this.props.loader) {
            return <h5 className='text-center'>Loader...</h5>
        }
        if (!this.props.movies.length) {
            return <h1 className="title ml-5 mt-5">Genre not found</h1>
        }
        return (
            <div className="row text-center">{movieList}</div>
        )
    }
}