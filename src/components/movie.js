import React from 'react';
import { api } from './api';
import axios from 'axios';

class Movie extends React.Component {
    youtubeUrl = `https://www.youtube.com/embed/`;

    constructor(props) {
        super(props);
        this.state = {
            movie: null,
            videos: []
        };

        this.back = this.back.bind(this);
    }

    componentDidMount() {
        const cancelToken = axios.CancelToken.source();
        const {id} = this.props.match.params;
        axios.get(`${api.apiHost}/movie/${id}?api_key=${api.apiKey}`,
            { cancelToken: cancelToken.token })
            .then(result => this.setState({movie: result.data}));

        axios.get(`${api.apiHost}/movie/${id}/videos?api_key=${api.apiKey}`,
            { cancelToken: cancelToken.token })
            .then(result => {
                result.data.results.forEach(item => {
                    if (item.site === 'YouTube') {
                        item.url = this.youtubeUrl + item.key;
                    }
                });
                this.setState({videos: result.data.results})
            });
    }

    render() {
        return (
            <div className="row offset-1 mt-5">
                <div className="col-md-3">
                    <iframe width="400" height="600" src={this.state.videos && this.state.videos[0] && this.state.videos[0].url} allowFullScreen/>
                </div>
                {this.state.movie && <div className="col-md-5">
                    <h2>{this.state.movie.title}</h2>
                    <h5>Status</h5>
                    <p>{this.state.movie.status}</p>
                    <h5>Tagline</h5>
                    <p>{this.state.movie.tagline}</p>
                    <h5>Overview</h5>
                    <p>{this.state.movie.overview}</p>
                    <h5>Popularity</h5>
                    <p>{this.state.movie.popularity}</p>
                    <button className='button' onClick={this.back}>Back</button>
                </div>}
            </div>
        );
    }

    back() {
        this.props.history.goBack();
    }
}

export default Movie;