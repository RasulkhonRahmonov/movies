import React from 'react';
import './App.css';
import {Switch, Route, withRouter} from 'react-router-dom'
import Movies from './components/movies';
import Pagination from './components/pagination';
import Movie from './components/movie';
import axios from 'axios';
import * as api from './components/api'

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            currentGenre: 0,
            genres: [],
            loader: false,
            page: 1,
            movies: []
        };

        this.filterMovies = this.filterMovies.bind(this);
        this.changePage = this.changePage.bind(this);
    }

    filterMovies(e) {
        this.setState({
            currentGenre: e.target.value,
            page: 1
        });
        if (window.location.pathname.length > 1) {
            this.props.history.push('/');
        }
    }

    changePage(e) {
        this.setState((state) => ({
            page: state.page + e
        }))
    }

    async  componentDidMount() {
        const cancelToken = axios.CancelToken.source();
        this.setState({loader: true});
        axios.get(api.api.apiHost + '/genre/movie/list?api_key=' + api.api.apiKey,
            { cancelToken: cancelToken.token })
            .then(res => {
                this.setState({genres: res.data.genres})
            });

        let url = `${api.api.apiHost}/discover/movie?api_key=${api.api.apiKey}&page=${this.state.page}`;
        if (this.state.currentGenre) {
            url += '&with_genres=' + this.state.currentGenre;
        }
        axios.get(url, { cancelToken: cancelToken.token })
            .then(result => {
                this.setState({loader: false});
                this.setState({movies: result.data.results});
            });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.currentGenre !== this.state.currentGenre || prevState.page !== this.state.page) {
            const cancelToken = axios.CancelToken.source();

            let url = `${api.api.apiHost}/discover/movie?api_key=${api.api.apiKey}&page=${this.state.page}`;
            if (this.state.currentGenre) {
                url += '&with_genres=' + this.state.currentGenre;
            }
            this.setState({loader: true});
            axios.get(url, { cancelToken: cancelToken.token })
                .then(result => {
                    this.setState({loader: false});
                    this.setState({movies: result.data.results});
                });
        }
    }

    render() {
        return (
            <div className="mt-5 mb-5">
                <div className="row offset-1">
                    <div className="col-10">
                        <div className="position-relative">
                            <select className='form-control' onChange={this.filterMovies} value={this.state.currentGenre}>
                                {this.state.genres && this.state.genres.length && this.state.genres.map(genre => (
                                    <option key={genre.id} value={genre.id}>{genre.name}</option>
                                ))}
                            </select>
                        </div>
                    </div>
                </div>
                <Switch>
                    <Route path='/' exact>
                        <Movies movies={this.state.movies} loader={this.state.loader}/>
                        {!this.state.loader &&<Pagination
                            page={this.state.page}
                            totalPage={500}
                            changePage={this.changePage}/>}
                    </Route>
                    <Route path={'/:id'} component={Movie}/>
                </Switch>
            </div>
        )
    }
}

export default withRouter(App);
